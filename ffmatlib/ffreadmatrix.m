%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FUNCTION NAME:
%   ffreadmatrix
%
% DESCRIPTION:
%    [Mat] = ffreadmatrix(filename)
%   Read CSR freefem matrices in sparse Matlab/Octave matrices.
%
% INPUT:
%   filename - <string> Path and name for saved data
%            - <int> number of matrices to load (optional)
%
% OUTPUT:
%   Mat - <cell> struct containing one or several matrices
%
% ASSUMPTIONS AND LIMITATIONS:
%   None
%
% REVISION HISTORY:
%   10/11/2021 Initial implementation: Pmollo
%   16/02/2021 Overloaded version: Pmollo
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Mat] = ffreadmatrix(filename, nbmat)
  % - Check number of matrices
  if exist('nbmat','var')==0
    nbmat=1;
    
    % - Initiate
    Mat = {nbmat,3};
    ifl = fopen(filename,'r');
    
    % - Loop over matrices
    N = 1;
    while(!feof(ifl))
      % Scan header
      tmp = fscanf(ifl,"%s",4);
      tm1 = fscanf(ifl,"%s",1);
      tmp = fscanf(ifl,"%s",7);
      
      % Scan size
      n   = fscanf(ifl,"%d",1);
      m   = fscanf(ifl,"%d",1);
      nz  = fscanf(ifl,"%d",1);
      tm2 = fscanf(ifl,"%d",4);
      
      % Load data
      I = zeros(nz,1);
      J = zeros(nz,1);
      V = zeros(nz,1);
      for Nbv=1:nz
        I(Nbv) = fscanf(ifl,"%d",1) + 1;
        J(Nbv) = fscanf(ifl,"%d",1) + 1;
        V(Nbv) = fscanf(ifl,"%e",1);
      end
      M = sparse(I,J,V,n,m);
      
	  if nnz(M)~=0
        Mat{N,1} = M;
        Mat{N,2} = tm1;
        Mat{N,3} = tm2;
	  end
      N = N+1;
    end
    fclose(ifl);

  else 
    % - Initiate
    Mat = {nbmat,3};
    ifl = fopen(filename,'r');
    
    % - Loop over matrices
    for N=1:nbmat
      % Scan header
      tmp = fscanf(ifl,"%s",4);
      tmp = fscanf(ifl,"%s",1);
      Mat{N,2} = tmp;
      tmp = fscanf(ifl,"%s",7);
      
      % Scan size
      n   = fscanf(ifl,"%d",1);
      m   = fscanf(ifl,"%d",1);
      nz  = fscanf(ifl,"%d",1);
      tmp = fscanf(ifl,"%d",4);
      Mat{N,3} = tmp;
      
      % Load data
      I = zeros(nz,1);
      J = zeros(nz,1);
      V = zeros(nz,1);
      for Nbv=1:nz
        I(Nbv) = fscanf(ifl,"%d",1) + 1;
        J(Nbv) = fscanf(ifl,"%d",1) + 1;
        V(Nbv) = fscanf(ifl,"%e",1);
      end
          
      Mat{N,1} = sparse(I,J,V,n,m);
    end
  
  end
  
end
