function [] = ffwritemeshSurface(msh, fileid)
  f = fopen(fileid,'w');
  
  fprintf(f,"MeshVersionFormatted 2 \n\n");
  fprintf(f,"Dimension 3\n\n");
  
  % --- Nodes
  fprintf(f,"Vertices\n");
  fprintf(f,"%d\n",size(msh.Nodes,1));
  for i=1:size(msh.Nodes,1)
    fprintf(f,"%16.15f %16.15f %16.15f %d\n", ...
    msh.Nodes(i,1), msh.Nodes(i,2), msh.Nodes(i,3), msh.LabelNodes(i) );
  end
  
  % --- Triangles
  fprintf(f,"\nTriangles\n");
  fprintf(f,"%d\n",size(msh.Elements,1))
  for i=1:size(msh.Elements,1)
    fprintf(f,"%d %d %d %d\n", ...
    msh.Elements(i,1), msh.Elements(i,2), msh.Elements(i,3), ...
    msh.LabelElements(i) );
  end
  
  % --- End
  fprintf(f,"\nEnd");  
  fclose(f);
end